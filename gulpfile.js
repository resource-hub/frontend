var path = require("path");

var gulp = require("gulp");
function pipe(src, file, trgt) {
  gulp.src(path.join(src, file)).pipe(gulp.dest(trgt));
}
function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}
var replace = require("gulp-string-replace");

var base = path.dirname(__filename);
var target = path.join(
  path.dirname(path.dirname(__filename)),
  "resource-hub",
  "src",
  "resource_hub",
  "static"
);

gulp.task("fullcalendar", function () {
  var src = path.join(base, "node_modules", "@fullcalendar");
  var trgt = path.join(target, "fullcalendar");
  console.log("Copying latest Fullcalendar to: " + trgt + " from " + src);

  // js
  gulp.src(path.join(src, "core", "main.min.js")).pipe(gulp.dest(trgt));
  // css
  gulp.src(path.join(src, "core", "main.min.css")).pipe(gulp.dest(trgt));

  // plugins
  gulp
    .src(path.join(src, "daygrid", "main.min.js"))
    .pipe(gulp.dest(path.join(trgt, "daygrid")));
  gulp
    .src(path.join(src, "daygrid", "main.min.css"))
    .pipe(gulp.dest(path.join(trgt, "daygrid")));
  gulp
    .src(path.join(src, "timegrid", "main.min.js"))
    .pipe(gulp.dest(path.join(trgt, "timegrid")));
  gulp
    .src(path.join(src, "timegrid", "main.min.css"))
    .pipe(gulp.dest(path.join(trgt, "timegrid")));
  gulp
    .src(path.join(src, "list", "main.min.js"))
    .pipe(gulp.dest(path.join(trgt, "list")));
  gulp
    .src(path.join(src, "list", "main.min.css"))
    .pipe(gulp.dest(path.join(trgt, "list")));

  // locales
  gulp
    .src([path.join(src, "core") + "/locales/**/*"])
    .pipe(gulp.dest(path.join(trgt, "locales")));
});

gulp.task("leaflet", function () {
  var src = path.join(base, "node_modules", "leaflet", "dist");
  var trgt = path.join(target, "leaflet");
  console.log("Copying latest Leaflet to: " + trgt + " from " + src);
  // js
  pipe(src, "leaflet.js", trgt);
  // css
  pipe(src, "leaflet.css", trgt);
});
gulp.task("photoswipe", function () {
  var src = path.join(base, "node_modules", "photoswipe", "dist");
  var trgt = path.join(target, "photoswipe");
  console.log("Copying latest Photowipe to: " + trgt + " from " + src);
  // js
  pipe(src, "photoswipe.min.js", trgt);
  pipe(src, "photoswipe-ui-default.min.js", trgt);
  gulp
    .src([src + "/default-skin/**/*"])
    .pipe(gulp.dest(path.join(trgt, "default-skin")));
  // css
  pipe(src, "photoswipe.css", trgt);
});

gulp.task("semantic", function () {
  var src = path.join(base, "semantic", "dist");
  var trgt = path.join(target, "semantic");
  console.log("Copying latest Semantic UI to: " + trgt + " from " + src);
  var google_fonts = escapeRegExp(
    "https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin"
  );
  // js
  gulp.src(path.join(src, "semantic.min.js")).pipe(gulp.dest(trgt));
  // css
  gulp
    .src(path.join(src, "semantic.min.css"))
    .pipe(replace(new RegExp(google_fonts, "g"), "../css/fonts.css"))
    .pipe(gulp.dest(trgt));
  // default theme
  gulp
    .src([path.join(src, "themes") + "/default/**/*"])
    .pipe(gulp.dest(path.join(trgt, "themes", "default")));
});

gulp.task("jquery", function () {
  var src = path.join(base, "node_modules", "jquery", "dist");
  var trgt = path.join(target, "jquery");
  console.log("Copying latest jQuery to: " + trgt + " from " + src);
  // js
  gulp.src(path.join(src, "jquery.min.js")).pipe(gulp.dest(trgt));
});

gulp.task("default", [
  "fullcalendar",
  "leaflet",
  "semantic",
  "jquery",
  "photoswipe",
]);
